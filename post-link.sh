if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper dual_regression fsl_glm fsl_mvlm fsl_regfilt fsl_sbca fsl_schurprod melodic melodicreport Melodic_gui
fi
