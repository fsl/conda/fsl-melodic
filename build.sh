#!/usr/bin/env bash

set -e

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

make insertcopyright

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

# Needed so that ciftilib gets found correctly
export PKG_CONFIG_PATH=$PREFIX/lib64/pkgconfig:$PREFIX/lib/pkgconfig

# Unresolved errors are occasionally experienced on macOS
# when compiled with optimisation, so we compile without
# optimisations (see fsl/conda/fsl-melodic!25 and !26).
export USRCXXFLAGS="-O0"

make
make install
